<?php
/*
	Author Mark Lane <mark@2100computerlane.net>
	Copyright 2016
	License GPL3.0

	Function Download Bounce List for Domain from Mailgun and Parse into CSV

	Delimiter use is "|"

	Requires Mailgu\Mailgun library from composer
		
		https://github.com/mailgun/mailgun-php
 	
	
*/

// This outputs error is any occur.
error_reporting(-1);
ini_set('display_errors', 'On');

// Include the Autoloader 
require 'vendor/autoload.php';
use Mailgun\Mailgun;

// Instantiate the client. Replace with your private key.
// Don't send private keys over email!!!
$mgClient = new Mailgun('key-****************************');

$domain = 'yourdomain'; 
$result = $mgClient->get($domain.'/bounces');


// http://stackoverflow.com/questions/22114888/mailgun-api-doesnt-output-json
// Response from Mailgun API needs to be encoded as json and then decoded. Go figure.

$jsondata=json_encode($result);

$jsonarray=json_decode($jsondata,true);


// Originally I used ',' as the delimiter but the date has commas in it so I switched to pipe '|'.
// To determine the structure of the response I printed the Json Array with print_r to get a formatted 
// print out of the array

print ("address|created_at|code|error\n");
foreach ($jsonarray['http_response_body']['items'] as $items) {
	print($items['address']."|".$items['created_at']."|".$items['code']."|".$items['error']."\n");
}



